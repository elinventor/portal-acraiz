Se habla del proceso actual

- El publicacdor maneja juntos archivos y Servidor OCSP

- Un pero para actualizar los archivos


- Están todos regados en un directorio

    * Estructura antigua
    PORTAL_OLD/
    └── sites
        └── default
            └── files
                ├── certificados
                ├── dpc
                └── lcr


- Excesivos Virtual host para redireccionar las rutas

- Dificil actualización del Front end
- Versión del drupal (CMS) en desuso (obsoleto) 
- Vulnerabilidad encontrada
- Problemas con reglas de firewall y url's



* Certificados
http://acraiz.suscerte.gob.ve/sites/default/files/certificados/CERTIFICADO-RAIZ-SHA384
http://acraiz.suscerte.gob.ve/sites/default/files/certificados/PSCFII-SHA256


* DPC
http://acraiz.suscerte.gob.ve/sites/default/files/dpc/DPC_AC_RAIZ_V3.3.pdf
http://acraiz.suscerte.gob.ve/sites/default/files/dpc/DPC-FII-2009.pdf

* LCR
http://acraiz.suscerte.gob.ve/sites/default/files/lcr/CERTIFICADO-RAIZ-SHA1CRL
http://acraiz.suscerte.gob.ve/sites/default/files/lcr/ACALTOS-FUNCIONARIOS-SHA256CRL.txt






Se hace un esquema de la descripción del proceso propuesto

Proceso:

- Se separó el servidor OCSP del publicador
    * Se creó y se reforzó un servidor virtual para el servicio OCSP
    * Una máquina virtual para el publicador
- Separada Base de datos de aplicación
    * Una máquina virtual para la DB
- Se generó estructura separada para los archivos 


media/
├── CERTIFICADOS
│   └── ACRAIZ
│       ├── CERTIFICADO-RAIZ-SHA1
│       ├── CERTIFICADO-RAIZ-SHA1.cer
│       ├── CERTIFICADO-RAIZ-SHA1.crt
│       └── CERTIFICADO-RAIZ-SHA1.txt
├── DPC
│   └── ACRAIZ
│       └── DPC_AC_RAIZ_V3.3.pdf
├── LCR
│   └── ACRAIZ
│       ├── CERTIFICADO-RAIZ-SHA1CRL
│       ├── CERTIFICADO-RAIZ-SHA1CRLDER.crl
│       ├── CERTIFICADO-RAIZ-SHA1CRLPEM.pem
│       └── CERTIFICADO-RAIZ-SHA1CRL.txt
├── LOGOS
│   └── suscerte_logo_small.png
└── OCSP
    ├── CERTIFICADO-RAIZ-SHA1CRL
    ├── CERTIFICADO-RAIZ-SHA1CRLDER.crl
    ├── CERTIFICADO-RAIZ-SHA1CRLPEM.pem
    └── CERTIFICADO-RAIZ-SHA1CRL.txt




Interfaz:
- Interfaz completamente renovada en HTML5, Css3 y Jquery
- Interfaz adaptativa desde resoluciones de 320×480 hasta 2600x1600
- Administración simple para las noticias
- Facilidad en la personalización del Front end

Funcional:
- Se actualizará el archivo LCR al OCSP desde la interfaz administrativa automáticamente
- Mejora en la definición estructura y categorización de las publicaciones 
- Publicación selectiva por:
    * Categoría
    * Proveedor
    * Autoridad Certificadora
    * Certificado
    * Documento por grupos (LCR, DPC y/o PC)
    * Documento por individual (LCR, DPC y/o PC)
- Manejo de fechas de publicación adaptadas a la realidad de SUSCERTE

Administración:
- Se generaron roles de Usuario para la publicación y administración
- Doble interfaz administrativa (vía frontend y vía Backend)




FALTA:

- [LISTO] Que el logo en el proveedor no sea requerido
- [LISTO] Cambiar campo del hash para que sea un archivo
- [LISTO] Que el campo de acronimo en modelo Proveedores no acepte espacios en blanco
- [LISTO] Que el campo de acronimo en modelo Proveedores esté en mayuscula
    
- [LISTO] Remover Tipos de LCR ese campo no se usa
- [LISTO] pre_save para remonbrar lcr y ceritficado
- Cambiar cintillo
- Activar históricos
- [no se va a hacer] Convertir los campor de fecha a datetime
- [LISTO] Quitar los campos de algortimo y fechas del formulario de admin

- [LISTO] Leer las fechas del archivo de la LCR
- [LISTO] Leer las fechas del archivo de los certificados
- [LISTO] del OID sacar el arlgoritmo de certificados
- [LISTO] del OID sacar el arlgoritmo de LCR
- [LISTO] Huella cargar la huella desde un archivo

- Modificar la estructura de directorios
- modificar la apariencia del admin
- Modificar el formulario de login del admin
- Colocar borde a lo de los certificados
- Agregar copia por ssh

- Agregar autenticación con certificado para la interfaz administrativa




## Procedimiento de carga de archivos

- respaldar toda media
- sobreescribir los archivos
** considerar ¿guardar una sóla LCR?



xen-create-image --hostname=correo --fs=ext4 --dhcp --size=1024Gb --memory=6Gb --swap=4Gb --arch=amd64 --dist=jessie

deb http://10.16.13.123/debian jessie main contrib non-free
