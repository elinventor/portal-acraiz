jQuery(document).ready(function($){


	var $timeline_block = $('.cd-timeline-block');

	//hide timeline blocks which are outside the viewport
	$timeline_block.each(function(){
		if($(this).offset().top > $('#cd-timeline').scrollTop()+$('#panel-redecho').height()*0.10) {
			$(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
		}
	});

	//on scolling, show/animate timeline blocks when enter the viewport
	$('#panel-redecho').on('scroll', function(){

		$timeline_block.each(function(){
			if( $(this).offset().top <= $('#panel-redecho').scrollTop()+$('#panel-redecho').height()*0.10 && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) {
				$(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
			}
		});
	});

});