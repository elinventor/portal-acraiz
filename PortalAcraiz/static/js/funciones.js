
function ConvierteFechaENaES(fechaEN){
/** 
* Funcion que recibe una fecha en formato ingles "2015-01-21" y la devuelve 
* en formato español "21 Ene 2015 " 
* Tiene que recibir: 
* - fecha en formato ingles (2015-01-21) 
* - formato de respuesta: 
* 1 - fecha en formato cadena 
* devuelve el mes numerico si hay algún error
*/ 
  valores = fechaEN.split("-"); 
  mes = valores[1];
  //  Decora el formato en que se encuenta el mes
  switch (mes){
    case '01':
    mes = 'Ene';
    break;
    case '02':
    mes = 'Feb';
    break;
    case '03':
    mes = 'Mar';
    break;
    case '04':
    mes = 'Abr';
    break;
    case '05':
    mes = 'May';
    break;
    case '06':
    mes = 'Jun';
    break;
    case '07':
    mes = 'Jul';
    break;  
    case '08':
    mes = 'Ago';
    break;
    case '09':
    mes = 'Sep';
    break;
    case '10':
    mes = 'Oct';
    break;
    case '11':
    mes = 'Nov';
    break;
    case '12':
    mes = 'Dic';
    break;

    default:
    mes = valores[1];
  }
  return valores[2]+' '+mes+' '+valores[0];
}
