# -*- coding: utf-8 -*-
from django.db import models

class Categorias(models.Model):
    '''
    Modelo para almacenar las categorias de publicación
    '''
    nombre_autoridad = models.CharField(max_length=100,
            verbose_name=u'Nombre de la Categoría')
    orden_categoria = models.ForeignKey('OrdenCategoria',
            verbose_name='orden',
            unique=True,
            help_text=u'orden en que aparecerá la categoría')
    publicar_cat = models.BooleanField(default=False,
            verbose_name=u'Publicar Categoría')

    class Meta:
        db_table = u'categoria'
        verbose_name = u'categoría'

    def __unicode__(self):
        return '%s' % (self.nombre_autoridad)


class OrdenCategoria(models.Model):
    '''
    Modelo para almacenar los órdenes de las categorías
    '''
    orden = models.PositiveIntegerField()

    class Meta:
        db_table = u'orden'
        verbose_name = u'orden'
        verbose_name_plural = u'orden'

    def __unicode__(self):
        return u'%s' % (self.orden)
