from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _
#from menu import *

class OcspLdapApp(CMSApp):
    name = _('Ocsp/Ldap')
    urls = ['ocsp_ldap.urls',]
    app_name = 'Ocsp/Ldap'

apphook_pool.register(OcspLdapApp)
