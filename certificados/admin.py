# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import ( TipoFormato, StatusCertificados,
        AutoridadesCertificadoras, FormatoCert, Certificados, Proveedores)
from dpc_pc.models import DPC, PC

admin.site.register(TipoFormato)
admin.site.register(StatusCertificados)


class DPCInline(admin.TabularInline):
    model = DPC
    extra = 0


class AutoridadesCertificadorasAdmin(admin.ModelAdmin):
    list_display = ('nombre','categoria','publicar', 'proveedor')
    list_filter = ('nombre','categoria','publicar', 'proveedor')
    inlines = [DPCInline]
admin.site.register(AutoridadesCertificadoras, AutoridadesCertificadorasAdmin)


class PCInline(admin.TabularInline):
    model = PC
    extra = 0


class FormatoCertInline(admin.TabularInline):
    model = FormatoCert
    extra = 0


class CertificadosAdmin(admin.ModelAdmin):
    #form = CertificadosForm
    fields = ('ac', 'status', 'etiqueta')
    list_display = ('ac', 'status', 'algoritmo', 'etiqueta', 'fecha_inicio', 'fecha_fin')
    list_filter = ('ac' ,'status', 'algoritmo', 'fecha_inicio', 'fecha_fin')
    inlines = [FormatoCertInline, PCInline]
admin.site.register(Certificados, CertificadosAdmin)


class ProveedoresAdmin(admin.ModelAdmin):
    list_display = ('nombre','acronimo', 'publicar')
    list_filter = ('nombre','publicar')
admin.site.register(Proveedores, ProveedoresAdmin)
