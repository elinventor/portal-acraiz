# -*- coding: utf-8 -*-
from django.shortcuts import render, HttpResponse
from django.views.generic.base import View, TemplateView
from .models import Certificados, FormatoCert, AutoridadesCertificadoras
from secciones.models import Categorias
from django.core import serializers
import json
import datetime


class CertificadosValidosView(View):
    template = 'acraiz/certificado_valido.html'
    diccionario = {}
    diccionario.update({'fecha_actual': datetime.date.today()})
    def get(self, request, *args, **kwargs):
        categorias = Categorias.objects.all().order_by('orden_categoria')
        self.diccionario.update({'categorias': categorias})

        if request.is_ajax():
            if request.GET:
                '''
                nombre_ac
                proveedor
                hash
                status
                formatos 
                huella
                fechas
                '''
                #listado_historial = FormatoCert.objects.filter(formato__formatocert__certificado__ac__id=request.GET['id']).order_by('-certificado__fecha_inicio')

                lista_certificados = Certificados.objects.filter(
                    ac__id=request.GET['id']).order_by('-fecha_inicio')

                import pdb
                #pdb.set_trace()

                #data = serializers.serialize('json', certificados, indent=4, extras=('get_formatos'),) # Funcion de FormatoCert

                certificados = {}
                for certificado in lista_certificados:
                    formatos = {}
                    tipo_formatos = FormatoCert.objects.filter(certificado=certificado)
                    for tipo in tipo_formatos:
                        formatos['formato_%s' % (tipo.id)] = {
                                'formato': tipo.formato.nombre,
                                'archivo': 'archivo',
                                'huella': tipo.huella,
                            }
                    certificados['certificado_%s' % (certificado.id)] = {
                            "pk": certificado.id,
                            "model": "certificados.certificados",
                            "fields": {
                                    "status": certificado.status.nombre,
                                    "ac": certificado.ac.nombre,
                                    "fecha_inicio": certificado.fecha_inicio.strftime('%d-%m-%Y'),
                                    "fecha_fin": certificado.fecha_fin.strftime('%d-%m-%Y'),
                                    "hash_cert": certificado.hash_cert.nombre,
                                    "etiqueta": certificado.etiqueta,
                                    "formatos": formatos, 
                                },
                        }

                return HttpResponse(json.dumps(certificados), content_type='application/json')
        return render(request,
            template_name=self.template,
            dictionary=self.diccionario,
        )
