from django.contrib import admin
from dpc_pc.models import DPC, PC, TipoDocumento


class PCAdmin(admin.ModelAdmin):
    list_display = ('etiqueta', 'fecha_publicacion')
    list_filter = ('etiqueta', 'fecha_publicacion')
admin.site.register(PC, PCAdmin)


class DPCAdmin(admin.ModelAdmin):
    list_display = ('etiqueta','huella', 'fecha_publicacion', 'fecha_caducidad', 'archivo', 'certificado')
    list_filter = ('etiqueta','huella', 'fecha_publicacion', 'fecha_caducidad', 'certificado')
admin.site.register(DPC, DPCAdmin)


class TipoDocumentoAdmin(admin.ModelAdmin):
    list_display = ('nombre', )
admin.site.register(TipoDocumento, TipoDocumentoAdmin)