from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _
#from menu import *

class DpcPcApp(CMSApp):
    name = _('Dpc/Pc')
    urls = ['dpc_pc.urls',]
    app_name = 'Dpc/Pc'

apphook_pool.register(DpcPcApp)
