# -*- coding: utf-8 -*-
# import Django
import os
from django.db import models

# import project packages
from core.models import NombreAbstract
from certificados.models import Certificados, AutoridadesCertificadoras


class TipoDocumento(NombreAbstract):
    '''
    Modelo para definir el tipo de documento (PC, DPC, etc)
    '''

    class Meta:
        verbose_name = u'Tipo de documento'
        verbose_name_plural = u'Tipos de documentos'


class Documento(models.Model):
    '''
    Modelos para los Documentos PC y DPC
    '''
    etiqueta = models.CharField(max_length=1000,
            verbose_name=u'Nombre del documeto',
            help_text=u'Etiqueta que se mostrará para este documento')

    huella = models.FileField(upload_to = 'DOCUMENTO/HUELLA')

    fecha_publicacion = models.DateField(
            verbose_name= u'Fecha de publicación',
            help_text=u'Fecha de publicación del documento')
    fecha_caducidad = models.DateField(
            verbose_name= u'Fecha de expiración',
            help_text=u'Fecha de expiración del documento')
    publicar = models.BooleanField(default=True)

    class Meta:
        abstract = True

    def __unicode__(self):
        return '%s - %s' % (self.etiqueta, self.fecha_publicacion)


class PC(Documento):


    def get_image_path(self, filename):
        
        proveerdor = (self.certificado.ac.proveedor.acronimo)
        autoridad = (self.certificado.ac)
        ruta = 'PC/%s/%s' % (proveerdor, autoridad)
        print ruta
        return os.path.join( ruta, filename)

    archivo = models.FileField(upload_to = get_image_path,
            null=True,
            blank=True,
            verbose_name=u'Archivo',
            help_text=u'Cargue aquí el archivo')

    certificado = models.ForeignKey(Certificados,
                help_text=u'Certificado al que está asociada esta PC')
    class Meta:
        db_table = 'pc'


class DPC(Documento):

    def get_image_path(self, filename):
        
        autoridad = (self.certificado.proveedor.acronimo)
        ruta = 'DPC/%s' % (autoridad)
        print ruta
        return os.path.join( ruta, filename)

    archivo = models.FileField(upload_to = get_image_path,
            null=True,
            blank=True,
            verbose_name=u'Archivo',
            help_text=u'Cargue aquí el archivo')


    certificado = models.ForeignKey(AutoridadesCertificadoras,
                verbose_name=u'AC',
                help_text=u'Autoridad Certificadora asociada')

    class Meta:
        db_table = 'dpc'
