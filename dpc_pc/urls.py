# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from dpc_pc.views import DpcPcView

urlpatterns = patterns('',
    url(r'^', DpcPcView.as_view()),
)
