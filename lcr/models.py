# -*- coding: utf-8 -*-
# import Django
import os
from django.db import models

# import project packages
from core.models import NombreAbstract
from certificados.models import TipoFormato, CertificadoAbstract


# pre_save
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

#from subprocess import call
import subprocess

# Librerías y funciones 
from lib.funciones import check_algoritmo, check_fecha

class TipoFormato(NombreAbstract):
    '''
    Modelo para definir el tipo de formato
    '''

    class Meta:
        verbose_name = u'Tipo de formato'
        verbose_name_plural = u'Tipos de formatos'


class TipoCert(NombreAbstract):
    '''
    Modelo para definir el tipo de LCR
    '''

    class Meta:
        verbose_name = u'Tipo de LCR'   
        verbose_name_plural = u'Tipos de LCR'


class Lcr(CertificadoAbstract):
    '''
    Datos de una LCR
    '''
    publicar = models.BooleanField(default=True,
            verbose_name=u'¿Publicar LCR?',
            help_text=u'¿Desea que se publiquen LCRs para esta AC?')
    class Meta:
        verbose_name = u'LCR'
        verbose_name_plural = 'LCR'


class FormatoLcr(models.Model):
    '''Diferentes formatos para cada LCR'''
    def get_image_path(self, filename):
        autoridad = (self.lcr.ac.proveedor.acronimo)
        ruta = 'LCR/%s' % (autoridad)
        return os.path.join( ruta, filename)

    lcr = models.ForeignKey(Lcr, verbose_name='LCR')
    formato = models.ForeignKey(TipoFormato,
            verbose_name=u'Formato',
            help_text=u'Formato que en el que se publicará la LCR',
            related_name='Formatos de la LCR')
    archivo = models.FileField(upload_to = get_image_path,
            blank=True,
            help_text=u'Cargue el archivo de la LCR correspondiente')
    huella = models.FileField(upload_to = 'LCR/HUELLA')

    class Meta:
        verbose_name = u'Formatos de la LCR'
        verbose_name_plural = u'Formatos de LCR'

    def __unicode__(self):
        return '%s' % (self.lcr)

@receiver(pre_save, sender=FormatoLcr)
def pre_handler(sender, instance, **kwargs):
    '''
    Esto está para que pongas la función de borrado y actualización de la que hablamos
    '''
    #print "Ruta antes"
    #ruta = "media/"+str(instance.archivo)
    #print ruta


@receiver(post_save, sender=FormatoLcr)
def post_handler(sender, instance, **kwargs):

    archivo_nombre = str(instance.archivo)
    ruta = 'media/'+archivo_nombre

    if archivo_nombre.__contains__('.txt'):
        instance.lcr.algoritmo = check_algoritmo(ruta)
        instance.lcr.fecha_inicio = check_fecha(ruta,"Last Update: ")
        instance.lcr.fecha_fin = check_fecha(ruta,"Next Update: ")
        instance.lcr.save(update_fields=["fecha_inicio"])
        instance.lcr.save(update_fields=["fecha_fin"])
        instance.lcr.save(update_fields=["algoritmo"])




    # Para hacer la actualización automática del OPSC
    ruta_origen = "media/"+str(instance.archivo)
    # Se actualiza el OCSP automáticamente
    ruta_destino = "media/OCSP/"
    os.system("rsync "+ruta_origen+" "+ruta_destino)


