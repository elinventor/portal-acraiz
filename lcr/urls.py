# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from lcr.views import LcrView

urlpatterns = patterns('',
    url(r'^', LcrView.as_view()),
)
