# -*- coding: utf-8 -*-
from django.shortcuts import render, HttpResponse
from django.views.generic.base import View, TemplateView
from certificados.models import AutoridadesCertificadoras
from lcr.models import Lcr, FormatoLcr

class LcrView(View):
    template = 'acraiz/lcr.html'
    diccionario = {}

    def get(self, request, *args, **kwargs):
        autoridades = AutoridadesCertificadoras.objects.all()
        self.diccionario.update({'autoridades': autoridades})

        return render(request, 
            template_name = self.template,
            dictionary = self.diccionario,
        )
