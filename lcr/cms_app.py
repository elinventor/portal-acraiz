from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _
#from menu import *

class LcrApp(CMSApp):
    name = _('Lcr/Crl')
    urls = ['lcr.urls',]
    app_name = 'lcr'

apphook_pool.register(LcrApp)
